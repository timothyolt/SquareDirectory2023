Inbox:

Further ideas I'm jotting down to get out of my head:
 * some colors and styles
 * refresh in the toolbar too for accessibility
 * dividers by letter
 * smooth scrolling by index letter
 * reactive sorting/filtering
 * detail view
 * tablet support