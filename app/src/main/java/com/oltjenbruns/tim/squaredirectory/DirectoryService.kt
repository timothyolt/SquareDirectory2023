package com.oltjenbruns.tim.squaredirectory

import kotlinx.serialization.Serializable
import retrofit2.http.GET
import retrofit2.http.Path

class DirectoryService(private val api: RetrofitDirectoryApi) {

    private var directory: Directory? = null

    fun invalidate() { directory = null }

    suspend fun load() = directory?.let(::ApiSuccess) ?: loadFromApi()

    private suspend fun loadFromApi(): ApiResult<Directory, String> {
        return try {
            ApiSuccess(directory(api.getEmployees().employees).also { directory = it })
        } catch (exception: Exception) {
            ApiFailure(exception.message ?: "Api Error")
        }
    }
}

interface RetrofitDirectoryApi {

    @GET("{collection}.json")
    suspend fun getEmployees(@Path("collection") employeeFile: Collection = Collection.Standard): EmployeeList

    @Serializable
    enum class Collection(private val serialName: String) {
        Standard("employees"),
        Malformed("employees_malformed"),
        Empty("employees_empty");

        override fun toString() = serialName
    }
}
