package com.oltjenbruns.tim.squaredirectory

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.oltjenbruns.tim.squaredirectory.databinding.ItemContactBinding
import com.squareup.picasso.Picasso

class DirectoryAdapter: RecyclerView.Adapter<DirectoryAdapter.ContactViewHolder>() {

    var employees: List<Employee> = emptyList()
        set(value) {
            field = value
            notifyDataSetChanged()
        }

    override fun getItemCount() = employees.size

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) = ContactViewHolder(parent)

    override fun onBindViewHolder(holder: ContactViewHolder, position: Int) = holder.bind(employees[position])

    class ContactViewHolder(private val binding: ItemContactBinding) : RecyclerView.ViewHolder(binding.root) {

        constructor(parent: ViewGroup) : this(
            ItemContactBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        )

        fun bind(employee: Employee) {
            binding.itemContactName.text = employee.fullName
            binding.itemContactEmail.text = employee.email
            binding.itemContactTeam.text = employee.team
            Picasso.get().load(employee.imageUri).into(binding.itemContactImage)
        }
    }
}
