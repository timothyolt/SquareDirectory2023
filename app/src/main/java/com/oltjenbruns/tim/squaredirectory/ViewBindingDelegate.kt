package com.oltjenbruns.tim.squaredirectory

import android.view.View
import android.view.ViewGroup
import androidx.activity.ComponentActivity
import androidx.core.view.children
import androidx.lifecycle.DefaultLifecycleObserver
import androidx.lifecycle.LifecycleOwner
import androidx.viewbinding.ViewBinding
import kotlin.properties.ReadOnlyProperty
import kotlin.reflect.KProperty

// Delegate property requires the unused parameter later
@Suppress("UnusedReceiverParameter")
inline fun <reified T : ViewBinding> ComponentActivity.viewBinding() = ViewBindingDelegate(T::class.java)

/**
 * I've implemented this pattern many times, but I really had to stop and think why this time.
 *
 * At some point, a couple years back I saw it on StackOverflow, but I can't remember where,
 * and my implementation has wandered pretty far by now
 *
 * See also ./docs/decisions/0001-use-custom-viewbinding-delegate.md
 */
class ViewBindingDelegate<T : ViewBinding>(
    private val bindingClass: Class<T>
) : ReadOnlyProperty<ComponentActivity, T> {

    private var viewBinding: T? = null

    override fun getValue(thisRef: ComponentActivity, property: KProperty<*>): T =
        viewBinding ?: newViewBinding(thisRef)

    private fun newViewBinding(thisRef: ComponentActivity) =
        bindingClass.bind(
            thisRef.findViewById<ViewGroup>(android.R.id.content)!!.children.first()
        ).also { newViewBinding ->
            viewBinding = newViewBinding
            thisRef.lifecycle.addObserver(ClearViewBindingOnDestroy())
        }

    private inner class ClearViewBindingOnDestroy : DefaultLifecycleObserver {
        override fun onDestroy(owner: LifecycleOwner) {
            viewBinding = null
            owner.lifecycle.removeObserver(this)
        }
    }

}

// all ViewBinding classes have a generated static method that returns an instance of the object
@Suppress("UNCHECKED_CAST")
fun <T : ViewBinding> Class<T>.bind(view: View): T = getMethod("bind", View::class.java).invoke(null, view) as T
