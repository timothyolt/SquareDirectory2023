package com.oltjenbruns.tim.squaredirectory

import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.flow.*
import kotlinx.coroutines.launch

// keep the workflow separate from the business logic and presentation so the rest is simple to understand
// I'm calling this a controller, but it could also be a ViewModel,
// and I might also consider making the Presenter private as things grow.
// But I definitely keep the presenter in a separate object for testability/maintainability
class DirectoryController(
    private val service: DirectoryService,
    private val presenter: DirectoryPresenter,
    private val scope: CoroutineScope
) {

    fun load() {
        presenter.startLoading()
        scope.launch {
            presenter.finishLoading(service.load())
        }
    }

    fun refresh() {
        service.invalidate()
        load()
    }

}

// this is my MVI View
// calling this a presenter so MVI View does not overload with Android View
class DirectoryPresenter {

    private val _state = MutableStateFlow(
        DirectoryViewState(
            isLoading = true,
            emptyMessage = DirectoryViewState.EmptyMessage.FirstLoad,
            errorMessage = null,
            employees = null
        )
    )
    val state: StateFlow<DirectoryViewState> = _state

    fun startLoading() {
        _state.update { it.copy(isLoading = true) }
    }

    fun finishLoading(result: ApiResult<Directory, String>) = when (result) {
        is ApiSuccess -> finishLoading(result.success)
        is ApiFailure -> finishLoading(result)
    }

    fun finishLoading(directory: Directory) {
        _state.update {
            it.copy(
                isLoading = false,
                emptyMessage = if (directory is Directory.NoEmployees) DirectoryViewState.EmptyMessage.NoEmployees else null,
                employees = if (directory is Directory.OneOrManyEmployees) directory.employees else null
            )
        }
    }

    fun finishLoading(result: ApiFailure<String>) {
        _state.update { it.copy(isLoading = false, errorMessage = result.failure) }
    }

    fun onErrorMessageShown() = _state.update { it.copy(errorMessage = null) }
}

data class DirectoryViewState(
    val isLoading: Boolean,
    val emptyMessage: EmptyMessage?,
    val errorMessage: String?,
    val employees: List<Employee>?
) {
    enum class EmptyMessage { FirstLoad, NoEmployees }
}
