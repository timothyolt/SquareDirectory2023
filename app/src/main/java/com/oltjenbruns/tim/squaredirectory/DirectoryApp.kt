package com.oltjenbruns.tim.squaredirectory

import android.app.Activity
import android.app.Application
import com.jakewharton.retrofit2.converter.kotlinx.serialization.asConverterFactory
import kotlinx.coroutines.MainScope
import kotlinx.serialization.json.Json
import okhttp3.MediaType
import retrofit2.Retrofit
import retrofit2.create

// if we wanted to use app tests,
// we could make this an interface and have our test app implement it to provide a coroutine scope
class DirectoryApp : Application() {
    val coroutineScope = MainScope()

    private val json = Json {
        ignoreUnknownKeys = true
    }
    private val retrofit = Retrofit.Builder()
        .baseUrl("https://s3.amazonaws.com/sq-mobile-interview/")
        .addConverterFactory(json.asConverterFactory(MediaType.get("application/json")))
        .build()
        .create<RetrofitDirectoryApi>()

    val directoryService = DirectoryService(retrofit)
}

val Activity.directoryApp get() = (application as DirectoryApp)
