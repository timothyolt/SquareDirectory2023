package com.oltjenbruns.tim.squaredirectory

// last directory design felt like too much
sealed interface Directory {
    object NoEmployees : Directory
    data class OneOrManyEmployees(val employees: List<Employee>) : Directory {
        init { require(employees.any()) }
    }
}

/**
 * considered an `operator fun invoke()` in the companion object of Directory,
 * but that's a little too obscure for this app
 */
fun directory(employeeList: List<Employee>) =
    if (employeeList.any()) Directory.OneOrManyEmployees(employeeList) else Directory.NoEmployees