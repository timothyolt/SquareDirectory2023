package com.oltjenbruns.tim.squaredirectory

import android.os.Bundle
import android.view.View
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.lifecycleScope
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.snackbar.Snackbar
import com.oltjenbruns.tim.squaredirectory.databinding.ActivityDirectoryBinding
import kotlinx.coroutines.flow.collectLatest

// keeping layout ID at the top helps keep layout easy to find when the file grows
class DirectoryActivity : AppCompatActivity(R.layout.activity_directory) {

    // See ADR-0001
    private val binding: ActivityDirectoryBinding by viewBinding()
    private val adapter = DirectoryAdapter()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val presenter = DirectoryPresenter()
        val controller = DirectoryController(directoryApp.directoryService, presenter, directoryApp.coroutineScope)

        controller.load()

        binding.directoryList.adapter = adapter

        lifecycleScope.launchWhenResumed {
            presenter.state.collectLatest { render(presenter, it) }
        }

        binding.directoryRefresh.setOnRefreshListener {
            controller.refresh()
        }
    }

    private fun render(presenter: DirectoryPresenter, state: DirectoryViewState) {
        binding.directoryRefresh.isRefreshing = state.isLoading
        binding.directoryNoEmployeesText.setMessage(state.emptyMessage)
        setEmployees(binding.directoryList, state.employees)
        if (state.errorMessage != null) {
            Snackbar.make(binding.root, state.errorMessage, Snackbar.LENGTH_LONG).show()
            presenter.onErrorMessageShown()
        }
    }

    private fun TextView.setMessage(emptyMessage: DirectoryViewState.EmptyMessage?) {
        if (emptyMessage != null) {
            visibility = View.VISIBLE
            setText(
                when (emptyMessage) {
                    DirectoryViewState.EmptyMessage.FirstLoad -> R.string.no_employees_first_load
                    DirectoryViewState.EmptyMessage.NoEmployees -> R.string.no_employees
                }
            )
        } else {
            visibility = View.GONE
        }
    }

    private fun setEmployees(recyclerView: RecyclerView, employees: List<Employee>?) = if (employees != null) {
        recyclerView.visibility = View.VISIBLE
        adapter.employees = employees
    } else {
        recyclerView.visibility = View.GONE
    }

}
