package com.oltjenbruns.tim.squaredirectory

import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

/**
 * I like to put new code in tests and extract that into the prod codebase rather than the other way around,
 * it helps me keep untested code out of prod
 */

// Employee data in the context of the directory, there may be other contexts outside the scope of this app
// as a contractor, I dislike Microsoft Teams, but I am inspired by their contact popup with how I visualize this data
@Serializable
data class Employee(
    @SerialName("full_name") val fullName: String,
    @SerialName("email_address") val email: String,
    @SerialName("team") val team: String,
    @SerialName("photo_url_small") val imageUri: String
)

@Serializable
data class EmployeeList(
    val employees: List<Employee>
)
