package com.oltjenbruns.tim.squaredirectory

sealed interface ApiResult<out S, out F>
data class ApiSuccess<out S>(val success: S) : ApiResult<S, Nothing>
data class ApiFailure<out F>(val failure: F) : ApiResult<Nothing, F>