package com.oltjenbruns.tim.squaredirectory

import org.junit.Test

import org.junit.Assert.*

/**
 * Test inbox
 *
 * many employees
 *
 * error
 *
 */

class DirectoryTest {
    @Test
    fun `single employee`() {
        val employees = listOf(Employee("Tim Oltjenbruns", "tim@oltjenbruns.com", "Omni", "image"))
        val directory = directory(employees)
        assertEquals(Directory.OneOrManyEmployees(employees), directory)
    }

    @Test
    fun `two employees, in order they were received`() {
        val employees = listOf(
            Employee("Tim Oltjenbruns", "tim@oltjenbruns.com", "Omni", "image"),
            Employee("Anna Oltjenbruns", "anna@oltjenbruns.com", "Omni", "image"),
        )
        val directory = directory(employees)
        assertEquals(Directory.OneOrManyEmployees(employees), directory)
    }

    @Test
    fun `no employees`() {
        val directory = directory(emptyList())
        assertEquals(Directory.NoEmployees, directory)
    }

    @Test
    fun `OneOrManyEmployees with no employees is a runtime error`() {
        assertThrows(IllegalArgumentException::class.java) {
            Directory.OneOrManyEmployees(emptyList())
        }
    }
}
