package com.oltjenbruns.tim.squaredirectory

import io.mockk.*
import kotlinx.coroutines.GlobalScope
import org.junit.Test

// on the fence about the value here, so I'll write them anyways
class DirectorControllerTest {

    @Test
    fun `load starts and finishes loading`() {
        val service: DirectoryService = mockk {
            coEvery { load() } returns ApiSuccess(Directory.NoEmployees)
        }
        val presenter: DirectoryPresenter = mockk {
            every { startLoading() } just runs
            every { finishLoading(any<ApiResult<Directory, String>>()) } just runs
        }
        val controller = DirectoryController(service, presenter, GlobalScope)

        controller.load()

        verifyOrder {
            presenter.startLoading()
            presenter.finishLoading(ApiSuccess(Directory.NoEmployees))
        }
    }

    @Test
    fun `refresh invalidates the service before loading`() {
        val service: DirectoryService = mockk {
            coEvery { load() } returns ApiSuccess(Directory.NoEmployees)
            every { invalidate() } just runs
        }
        val presenter: DirectoryPresenter = mockk {
            every { startLoading() } just runs
            every { finishLoading(any<ApiResult<Directory, String>>()) } just runs
        }
        val controller = DirectoryController(service, presenter, GlobalScope)

        controller.refresh()

        verifyOrder {
            service.invalidate()
            presenter.startLoading()
            presenter.finishLoading(ApiSuccess(Directory.NoEmployees))
        }
    }

}
