package com.oltjenbruns.tim.squaredirectory

import io.mockk.*
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.runBlocking
import org.junit.Assert.*
import org.junit.Test

class DirectorServiceTest {

    @Test
    fun `second service load caches`() {
        val api: RetrofitDirectoryApi = mockk {
            coEvery { getEmployees() } returns EmployeeList(emptyList())
        }
        val service = DirectoryService(api)

        runBlocking {
            service.load()
            service.load()
        }

        coVerifySequence {
            api.getEmployees()
        }
    }

    @Test
    fun `service load after invalidate does not cache`() {
        val api: RetrofitDirectoryApi = mockk {
            coEvery { getEmployees() } returns EmployeeList(emptyList())
        }
        val service = DirectoryService(api)

        runBlocking {
            service.load()
            service.invalidate()
            service.load()
        }

        coVerifySequence {
            api.getEmployees()
            api.getEmployees()
        }
    }

    @Test
    fun `service catches exceptions`() {
        val api: RetrofitDirectoryApi = mockk {
            coEvery { getEmployees() } throws Exception("error")
        }
        val service = DirectoryService(api)

        val result = runBlocking {
            service.load()
        }
        assertEquals(ApiFailure("error"), result)
    }

}
