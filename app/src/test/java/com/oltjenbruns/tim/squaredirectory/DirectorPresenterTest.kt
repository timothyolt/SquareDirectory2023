package com.oltjenbruns.tim.squaredirectory

import org.junit.Assert.*
import org.junit.Test

class DirectorPresenterTest {

    /**
     * it is the consumer's responsibility to call load,
     * load is true at creation so that there's still something to show the user
     * in the case that actually calling load is delayed
     */
    @Test
    fun `loading at creation`() {
        // might graduate to MVI-like but something MVC-like seems simple enough for now
        val presenter = DirectoryPresenter()

        // I use assertEquals when the SUT is a boolean, and assertTrue/False when I'm asserting some complex test logic
        assertEquals(true, presenter.state.value.isLoading)
    }

    @Test
    fun `stops loading after load`() {
        val presenter = DirectoryPresenter()

        presenter.finishLoading(directory(emptyList()))

        assertEquals(false, presenter.state.value.isLoading)
    }

    @Test
    fun `employees are accessible after loading`() {
        val presenter = DirectoryPresenter()
        val employees = listOf(Employee("Tom Sawyer", "tom@sawyer.com", "Omni", "image"))

        presenter.finishLoading(directory(employees))

        assertEquals(employees, presenter.state.value.employees)
    }

    @Test
    fun `employees are missing before loading`() {
        val presenter = DirectoryPresenter()

        assertEquals(null, presenter.state.value.employees)
    }

    @Test
    fun `first load message visible before loading`() {
        val presenter = DirectoryPresenter()

        assertEquals(DirectoryViewState.EmptyMessage.FirstLoad, presenter.state.value.emptyMessage)
    }

    @Test
    fun `no employees message visible after loading empty list`() {
        val presenter = DirectoryPresenter()

        presenter.finishLoading(directory(emptyList()))

        assertEquals(DirectoryViewState.EmptyMessage.NoEmployees, presenter.state.value.emptyMessage)
    }

    @Test
    fun `no message visible after loading at least one employee`() {
        val presenter = DirectoryPresenter()

        presenter.finishLoading(
            directory(
                listOf(
                    Employee("Tom Sawyer", "tom@sawyer.com", "Omni", "image")
                )
            )
        )

        assertEquals(null, presenter.state.value.emptyMessage)
    }

    @Test
    fun `second load shows loader again`() {
        val presenter = DirectoryPresenter()

        presenter.finishLoading(directory(emptyList()))
        presenter.startLoading()

        assertEquals(true, presenter.state.value.isLoading)
    }

    @Test
    fun `loader still clears on failures`() {
        val presenter = DirectoryPresenter()

        presenter.finishLoading(ApiFailure("error"))

        assertEquals(false, presenter.state.value.isLoading)
    }

    @Test
    fun `error message shows on failure`() {
        val presenter = DirectoryPresenter()

        presenter.finishLoading(ApiFailure("error"))

        assertEquals("error", presenter.state.value.errorMessage)
    }

    @Test
    fun `error message clears when acknowledged`() {
        val presenter = DirectoryPresenter()

        presenter.finishLoading(ApiFailure("error"))
        presenter.onErrorMessageShown()

        assertEquals(null, presenter.state.value.errorMessage)
    }

    @Test
    fun `no error message on start`() {
        val presenter = DirectoryPresenter()

        assertEquals(null, presenter.state.value.errorMessage)
    }
}
