---
status: accepted

---
# Use Custom ViewBinding Delegate

## Context and Problem Statement

I think ViewBinding is great, but attaining/retaining a copy of a ViewBinding object while respecting the 
Android Lifecycle can be tricky.

{Describe the context and problem statement, e.g., in free form using two to three sentences or in the form of an illustrative story.
 You may want to articulate the problem in form of a question and add links to collaboration boards or issue management systems.}

<!-- This is an optional element. Feel free to remove. -->
## Decision Drivers

* Trying to use ViewBinding too early can cause a crash
* Holding on to ViewBinding too late can cause a memory leak
* I want to support configuration changes, which means new ViewBinding layouts throughout lifecycle

## Considered Options

* Custom ViewBinding Delegate
* Kotlin Lateinit
* Kotlin lazy
* Kotlin nullable

## Decision Outcome

Chosen option: Custom ViewBinding Delegate, because
this puts it all lifecycle handling of ViewBinding in one place.

The other options either would not support configuration changes, 
or would require repeatedly remembering to cleanup the ViewBinding in each onDestroy event.

### Example
```kotlin
class DirectoryActivity : AppCompatActivity(R.layout.activity_directory) {

    private val binding: ActivityDirectoryBinding by viewBinding()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding.directoryList.adapter = DirectoryAdapter(listOf("Hello!", "World!"))
    }

}
```

<!-- This is an optional element. Feel free to remove. -->
### Consequences

* Good, because I get all of the desired traits I identified
* Good, because if I change my mind, I can change the implementation later
* Good, because onCreate isn't required if no additional setup is needed
* Good, because the layout being used can be easy to find after the file grows
* Bad, because the layout used is kept in two places
* Bad, because its pretty complicated, including using reflection and a lot more code than other alternatives

## Pros and Cons of the Options

### Kotlin Lateinit

```kotlin
class DirectoryActivity : AppCompatActivity() {

   private lateinit var binding: ActivityDirectoryBinding

   override fun onCreate(savedInstanceState: Bundle?) {
      super.onCreate(savedInstanceState)
      binding = ActivityDirectoryBinding.inflate(layoutInflater)
      setContentView(binding.root)
    
      binding.directoryList.adapter = DirectoryAdapter(listOf("Hello!", "World!"))
   }

}
```

* Good, because this is exactly how it's done in the Android Docs
* Good, because it's simple
* Good, because it keeps the layout used in one place
* Bad, because the layout being used can be hard to find after the file grows
* Bad, because the View is held onto after onDestroy, causing a memory leak

### Kotlin lazy

```kotlin
class DirectoryActivity : AppCompatActivity(R.layout.activity_directory) {

    private val binding by lazy { 
        ActivityDirectoryBinding.bind(findViewById<ViewGroup>(android.R.id.content)!!.children.first()) 
    }
    
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding.directoryList.adapter = DirectoryAdapter(listOf("Hello!", "World!"))
    }

}
```

* Good, because this uses a well-known Kotlin delegate function
* Good, because it's still simpler than the Custom ViewBinding Delegate solution
* Good, because the layout being used can be easy to find after the file grows
* Good, because onCreate isn't required if no additional setup is needed
* Bad, because the layout used is kept in two places
* Bad, because the View is held onto after onDestroy, causing a memory leak

### Kotlin nullable

```kotlin
class DirectoryActivity : AppCompatActivity() {
 
    private var binding: ActivityDirectoryBinding? = null
 
    override fun onCreate(savedInstanceState: Bundle?) {
       super.onCreate(savedInstanceState)
       val binding = ActivityDirectoryBinding.inflate(layoutInflater)
       this.binding = binding
       setContentView(binding.root)

       binding.directoryList.adapter = DirectoryAdapter(listOf("Hello!", "World!"))
    }

    override fun onDestroy() {
       super.onDestroy()
       binding = null
    }

}
```

* Good, because it's simple
* Good, because it handles the onDestroy
* Good, because it keeps the layout used in one place
* Bad, because the layout being used can be hard to find after the file grows
* Bad, because it's a lot of repeated noise
* Bad, because I will forget to do some part of this pattern in a later Activity/Fragment
* Bad, because when I change my mind, I need to change in a lot of places

## More Information

See also: [ViewBindingDelegate.kt](../../app/src/main/java/com/oltjenbruns/tim/squaredirectory/ViewBindingDelegate.kt)
