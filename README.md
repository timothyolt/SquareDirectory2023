# SquareDirectory2023

## Build tools & versions used
 * Android SDK 24
 * Gradle 7.5
 * Kotlin 1.8.0
 * Android Gradle Plugin 7.3.0
 * IntelliJ Ultimate 2023.1

## Steps to run the app
 * Start emulator/connect device and test connection using `adb devices`
 * Either A: Hit the run button in the InteliJ/Android Studio IDE
 * Or B: run in a terminal `./gradlew app:installDebug && adb shell am start -n com.oltjenbruns.tim.squaredirectory/com.oltjenbruns.tim.squaredirectory.DirectoryActivity`

## What areas of the app did you focus on?
 * Layouts tested on Pixel 6 Pro
 * Development Process
   * Documentation
     * Recording my train of thought so the reader knows what its like collaborating with me
     * Atomic commits (keeping up with docs and tests along the way)
     * Decision records
   * Implementing only what I need, when I need it
   * MVI/Unidirectional data flow

## What was the reason for your focus? What problems were you trying to solve?
This is a simple app, but for the purposes of this we can act like it will grow.
I did not want to overcomplicate things too early, but I did want to leave it in an easily maintainable

## How long did you spend on this project?
About 5-6 hours. 
Out of nowhere I had to help my current client remediate some security issues, so I had to squeeze this in wherever I could.

## Did you make any trade-offs for this project? What would you have done differently with more time?
Of course! I might change my Custom ViewBinding back to something more simple, 
you can see the decision record in docs/decisions

## What do you think is the weakest part of your project?
The UI/UX. In hindsight, I should have skipped the custom ViewBinder, which would have afforded me more time to polish the UX.

## Did you copy any code or dependencies? Please make sure to attribute them here!
I "copied" some patterns out of my head that I frequently play around with on my personal projects.
Not totally copy-paste, but best to let you know they weren't 100% organic!
 * [ViewBindingDelegate](./app/src/main/java/com/oltjenbruns/tim/squaredirectory/ViewBindingDelegate.kt) was copied from StackOverflow and evolved, see file for more details
 * [ApiResult](./app/src/main/java/com/oltjenbruns/tim/squaredirectory/ApiResult.kt) was copied from StackOverflow and evolved, see file for more details

## Is there any other information you’d like us to know?
See [inbox.md](./inbox.md) for priorities and future thoughts as I get them

Please also take a look at my commit history! I put a lot of consideration into documenting my thoughts there.
